/*
 Part of Hanjie3D project
 
 Released under the GPL 2.0 License (See file gpl-2.0.txt or http://www.gnu.org/licenses/gpl-2.0.txt)
 Copyright (2006 - 2011)
 Author: Omar Baqueiro Epinosa

*/
#ifndef GRAPHICENGINE_H
#define GRAPHICENGINE_H

#include <gl/gl.h>
#include <gl/glu.h>
#include <SDL/SDL.h>

#define STR_TTF_FONT_FILE "Cardo98s.ttf"


/*
 * Manages all the graphic routines
 */
class GraphicEngine
{
	private:

        float FPS; // frame per seconds
		// class constructors are private
		GraphicEngine(){}
		GraphicEngine(GraphicEngine const&){}
		GraphicEngine& operator=(GraphicEngine const&){ return get_handle();}

		SDL_Surface *screen;

		// Set OpenGL starting configuration
		void setup_opengl();

		// Setup OpenGL lights
		void setup_lights();

		/* Quick utility function for texture creation */
		static int power_of_two(int input);
        void update_FPS();
	public:

		static const int SCREEN_WIDTH = 800;
		static const int SCREEN_HEIGHT = 600;
		static const int SCREEN_BPP= 32;

		static GraphicEngine & get_handle();

		// class destructor
		~GraphicEngine();

		// initializee SDL and OpenGL graphics engines
		int init_graphics(bool fullscreen);

		// draw current scenario frame to the screen
		void draw_scene(void);

		float get_FPS();
		// load a bitmap image from file
		static SDL_Surface *load_bitmap(char *filename);

		// Create a GL texture from a SDL surface
		static GLuint load_texture(SDL_Surface *surface, GLfloat *texcoord);

		// Set the OpenGL active texture to use
		static void enable_texture(GLuint texture);

		// generates a texture containing the specified text
		static GLuint generate_GL_text(char *text, GLuint texId);


};


#endif // GRAPHICENGINE_H
