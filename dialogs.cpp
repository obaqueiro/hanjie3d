/*
 Part of Hanjie3D project
 
 Released under the GPL 2.0 License (See file gpl-2.0.txt or http://www.gnu.org/licenses/gpl-2.0.txt)
 Copyright (2006 - 2011)
 Author: Omar Baqueiro Epinosa

*/

#if defined(__GNUG__) && !defined(__APPLE__)
#pragma implementation "dialogs.h"
#endif

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "dialogs.h"

////@begin XPM images

////@end XPM images

/*!
 * Application instance implementation
 */

////@begin implement app
IMPLEMENT_APP( DialogsApp )
////@end implement app

/*!
 * DialogsApp type definition
 */

IMPLEMENT_CLASS( DialogsApp, wxApp )

/*!
 * DialogsApp event table definition
 */

BEGIN_EVENT_TABLE( DialogsApp, wxApp )

////@begin DialogsApp event table entries
////@end DialogsApp event table entries

END_EVENT_TABLE()

/*!
 * Constructor for DialogsApp
 */

DialogsApp::DialogsApp()
{
////@begin DialogsApp member initialisation
////@end DialogsApp member initialisation
}

/*!
 * Initialisation for DialogsApp
 */

bool DialogsApp::OnInit()
{    
////@begin DialogsApp initialisation
    // Remove the comment markers above and below this block
    // to make permanent changes to the code.

#if wxUSE_XPM
    wxImage::AddHandler( new wxXPMHandler );
#endif
#if wxUSE_LIBPNG
    wxImage::AddHandler( new wxPNGHandler );
#endif
#if wxUSE_LIBJPEG
    wxImage::AddHandler( new wxJPEGHandler );
#endif
#if wxUSE_GIF
    wxImage::AddHandler( new wxGIFHandler );
#endif
////@end DialogsApp initialisation

    return true;
}

/*!
 * Cleanup for DialogsApp
 */
int DialogsApp::OnExit()
{    
////@begin DialogsApp cleanup
    return wxApp::OnExit();
////@end DialogsApp cleanup
}

