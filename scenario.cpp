/*
 Part of Hanjie3D project
 
 Released under the GPL 2.0 License (See file gpl-2.0.txt or http://www.gnu.org/licenses/gpl-2.0.txt)
 Copyright (2006 - 2011)
 Author: Omar Baqueiro Epinosa

*/

#include "scenario.h" // class's header file
#include "camera.h"
#include "scenario3d.h"
#include <iostream>

// class constructor
Scenario::Scenario()
{
		rotationMatrix[0][0]=1;
		rotationMatrix[0][1]=0;
		rotationMatrix[0][2]=0;

		rotationMatrix[1][0]=0;
		rotationMatrix[1][1]=1;
		rotationMatrix[1][2]=0;

		rotationMatrix[2][0]=0;
		rotationMatrix[2][1]=0;
		rotationMatrix[2][2]=-1;


		centerX=centerY=centerZ=0;
		rotx=roty=rotz=0;
		deltaX=deltaY=deltaZ=0;
		cubeSpace=CUBE_SPACE;

		ambientLight[0]= 1.0;
		ambientLight[1]= 1.0;
		ambientLight[2]= 1.0;
		ambientLight[3]= 1.0;

		lightPosition[0]=0.0f;
		lightPosition[1]=0.0f;
		lightPosition[2]=0.0f;
		lightPosition[3]=1.0f;
		showCubeType=false;

		}

// class destructor
Scenario::~Scenario()
{
	// Check for the scenario data, if it is available free memory
	if (this->cubesData){
		for (int i=0;i<this->width;i++)	{
			if (cubesData[i]){
				for (int j=0;j<this->height;j++){
					if (cubesData[i][j]){
						for (int k=0;k<this->depth;k++){
							delete cubesData[i][j][k];
						}
						delete [] cubesData[i][j];
					}
				}
				delete[] cubesData[i];
			}

		}

	}
}

Scenario &Scenario::get_handle()
{
		static Scenario s ; // the only scenario instance
		return s;
}
// Loads a new scenario from file
bool Scenario::load_scenario(char *filename)
{
	int fHeight, fWidth, fDepth;
	char *fileData;

	FILE *file;

	// open the file
	file = fopen(filename, "r");

	if (file==NULL)
	{
		return false;
	}

	// read file data


	fWidth = fgetc(file); // read scenario width;
	fHeight = fgetc(file); // read scenario height;
	fDepth = fgetc(file); //read scenario depth;


	fileData = new char[fWidth*fHeight];
	int bytesread =fread (fileData, 1, fWidth*fHeight, file); // read data from file;

	fclose(file); // close file

	// create scenario virtual structure

	cubesData = new VirtualCube***[fWidth]; //allocate space for the scenario information
	for (int i=0; i<fWidth;i++)
	{
		cubesData[i] = new VirtualCube**[fHeight];
	}
	if (cubesData==NULL)
	{
		return false;
	}
	char *ptr = fileData; // temporal pointer to the data buffer
	// traverse the fileData information and create the scenario structure

		for (int j=fHeight-1;j>=0;j--)
		{
		    for (int i=0; i<fWidth;i++)
            {
			// create the array containing the scenario cubes information(depth)
			cubesData[i][j] = new VirtualCube*[fDepth];
			// get the depth line cubes information
			int depthInfo = (int) *ptr;
			ptr++;

			int testbit=1; // tests if the bit is 1 or 0 to obtain the state of the cube
			for (int k = fDepth-1;k>=0;k--)
			{
				VirtualCube *newCube = new VirtualCube;
				cubesData[i][j][k]=newCube;
				if (testbit&depthInfo) //if the bit is set then the cube is part of the image
				{
					newCube->type=TYPE_USED;
				}
				else // else the cube is not part of the image
				{
						newCube->type=TYPE_UNUSED;
				}
					newCube->state = STATE_NORMAL;
				testbit=testbit<<1;
			}
		}
	}
	depth = fDepth;
	width= fWidth;
	height = fHeight;

	cursor.set_z(depth-1);

	// free the used variables
	delete [] fileData;


	print_cubes_data();
	return true;
}


void Scenario::print_cubes_data()
{
	for (int z=0;z<depth;z++)
	{
		for (int j=0;j<height;j++)
		{
			for (int i=0;i<width;i++)
			{
				printf("%d ",cubesData[i][j][z]->type);

			}
			putchar ('\n');
		}
		putchar ('\n');
	}
}
void Scenario::print_rotation_matrix()
{

	char text[40];

	for (int i=0;i<3;i++){
		for (int j=0;j<3;j++){
			sprintf(text,"%d ",rotationMatrix[i][j]);

			std::cout << rotationMatrix[i][j]<<" ";
		}


		std::cout << "\n";
	}

	std::cout<<"\n";

}


void Scenario::swap_axis(int axis1[3], int axis2[3])
{
	for (int i=0;i<3;i++)
	{
		int temp = axis1[i];
		axis1[i]=axis2[i];
		axis2[i]=-temp;
	}
}

void Scenario::rotate_x(int direction)
{
	Scenario &scenario = Scenario::get_handle();
	Rotation newRot;
	// if there is currently a rotation ignore the petition
	if (deltaX!=0 || deltaY!=0 || deltaZ!=0 ) {return;}
	if (direction >0){ direction =1;}
	else if (direction<0) {direction = -1;}


	this->deltaX=90*direction*rotationMatrix[0][0];
	this->deltaY=90*direction*rotationMatrix[0][1];
	this->deltaZ=90*direction*rotationMatrix[0][2];
	//this->deltaX=90*direction;

	if (direction==1){
		swap_axis(rotationMatrix[1],rotationMatrix[2]);
	}
	else {
			swap_axis(rotationMatrix[2],rotationMatrix[1]);
	}


	update_hints(true);



}

void Scenario::rotate_y(int direction)
{
	Scenario &scenario = Scenario::get_handle();
	// if there is currently a rotation ignore the petition
	if (deltaX!=0 || deltaY!=0 || deltaZ!=0) {return;}
	if (direction >0){ direction =1;}
	else if (direction<0) {direction = -1;}
		this->deltaX=90*direction*rotationMatrix[1][0];
	this->deltaY=90*direction*rotationMatrix[1][1];
	this->deltaZ=90*direction*rotationMatrix[1][2];

	//this->deltaY=90*direction;
	if (direction==-1){
		swap_axis(rotationMatrix[0],rotationMatrix[2]);
	}
	else {
			swap_axis(rotationMatrix[2],rotationMatrix[0]);
	}
    update_hints(true);
}


void Scenario::rotate_z(int direction)
{

Scenario &scenario = Scenario::get_handle();
	// if there is currently a rotation ignore the petition
	if (deltaZ!=0) {return;}
	if (direction >0){ direction =1;}
	else if (direction<0) {direction = -1;}
	this->deltaZ=90*direction;
	if (direction==-1){
		swap_axis(rotationMatrix[0],rotationMatrix[1]);
	}
	else {
			swap_axis(rotationMatrix[1],rotationMatrix[0]);
	}
	print_rotation_matrix();
}

void Scenario::update_rotation()
{
	Scenario3D &scenario3D = Scenario3D::get_handle();
	if (deltaX >0){
		rotx+=ROTATION_STEP;  // update scenario coordinates
		deltaX-=ROTATION_STEP;   // set new delta value
		if (deltaX<0)            // if the positive rotation was done
			deltaX=0;            // set delta to 0
		// update scenarioGL rotation matrix
		scenario3D.rotate_x(ROTATION_STEP);

	}
	else if (deltaX<0){
		rotx-=ROTATION_STEP;
		deltaX+=ROTATION_STEP;   // set new delta value
		if (deltaX>0)            // if the negative rotation was done
				deltaX=0;            // set delta to 0
		// update scenarioGL rotation matrix
		scenario3D.rotate_x(-ROTATION_STEP);
	}
	// reset rotation when achieved a full circle
	if (rotx==360 || rotx==-360){
			rotx=0;
	}

	if (deltaY >0){
		roty+=ROTATION_STEP;  // update scenario coordiinates
		deltaY-=ROTATION_STEP;   // set new delta value
		if (deltaY<0){            // if the positive rotation was done
			deltaY=0;            // set delta to 0
		}
		// update scenarioGL rotation matrix
		scenario3D.rotate_y(ROTATION_STEP);
	}
	if (deltaY<0){
		roty-=ROTATION_STEP;  // update scenario coordiinates
		deltaY+=ROTATION_STEP;   // set new delta value
		if (deltaY>0){         // if the negative rotation was done
			deltaY=0;            // set delta to 0
		}
		// update scenarioGL rotation matrix
		scenario3D.rotate_y(-ROTATION_STEP);
	}

	// reset rotation when achieved a full circle
	if (roty==360 || roty==-360){
		roty=0;
	}

	if (deltaZ>0){
		rotz+=ROTATION_STEP;  // update scenario coordinates
		deltaZ-=ROTATION_STEP;   // set new delta value
		if (deltaZ<0){            // if the positive rotation was done
			deltaZ=0;            // set delta to 0
		}
		// update scenarioGL rotation matrix
		scenario3D.rotate_z(ROTATION_STEP);
	}
	if (deltaZ<0){
		rotz-=ROTATION_STEP;  // update scenario coordinates
		deltaZ+=ROTATION_STEP;   // set new delta value
		if (deltaZ>0){            // if the negative rotation was done
			deltaZ=0;            // set delta to 0
		}
		// update scenarioGL rotation matrix
		scenario3D.rotate_z(-ROTATION_STEP);
	}
		// reset rotation when achieved a full circle
	if (rotz==360 || rotz==-360){
		rotz=0;
	}

}
void Scenario::increment_cube_space()
{
	if (cubeSpace<6){
		this->cubeSpace+=0.1;
	}
}

void Scenario::decrement_cube_space()
{
	if (cubeSpace>0){
		this->cubeSpace-=0.1;
	}
}

 // returns the number of cubes on X axis
int Scenario::get_width()
{
	return this->width;
}
int Scenario::get_height()
{
	return this->height;
}
int Scenario::get_depth()
{
	return this->depth;
}

void Scenario::toggle_cubestate()
{
	VirtualCube *cube =
		cubesData[cursor.get_x()][cursor.get_y()][cursor.get_z()];
		switch (cube->state){
			case STATE_NORMAL:
				cube->state=STATE_X_CUBE;
				break;
			case STATE_X_CUBE:
				cube->state=STATE_O_CUBE;
				break;
			case STATE_O_CUBE:
				cube->state=STATE_NORMAL;
				break;
			}
}
void Scenario::set_cubestate(CubeState newState)
{
		cubesData[cursor.get_x()][cursor.get_y()][cursor.get_z()]->state= newState;
}

void Scenario::update()
{
	// set the position of the center of the scenario
	centerX=(width*CUBE_SIZE+(width-1)*cubeSpace)/2.0;
	centerY=(height*CUBE_SIZE+(height-1)*cubeSpace)/2.0;
	centerZ=(depth*CUBE_SIZE+(depth-1)*cubeSpace)/2.0;

	// update delta rotation values
	update_rotation();
	update_hints(false);
}
int Scenario::get_rotx()
{
	return this->rotx;
}

int Scenario::get_roty()
{
	return this->roty;
}

int Scenario::get_rotz()
{
	return this->rotz;
}

float Scenario::get_centerx()
{
	return this->centerX;
}

float Scenario::get_centery()
{
	return this->centerY;
}

float Scenario::get_centerz()
{
	return this->centerZ;
}

float Scenario::get_cubesize()
{
	return CUBE_SIZE;
}
float Scenario::get_cubespace()
{
	return cubeSpace;
}
VirtualCube & Scenario::get_cube(int i, int j, int k)
{
	if (i<width && j<height && k<depth &&
	    i>=0 && j >= 0 && k >= 0) {
			return *cubesData[i][j][k];
		}
}

Cursor &Scenario::get_cursor()
{
	return cursor;
}

void Scenario::update_hints(bool force)
{
	using std::deque;
    static int lastMat[3]={-1,-1,-1}; // the last Z rotation matrix

    // check if the rotation has been modified
    if (lastMat[0]==rotationMatrix[2][0] &&
        lastMat[1]==rotationMatrix[2][1] &&
        lastMat[2]==rotationMatrix[2][2] )
        {
            // if the rotation has not been modified, there is no need to
            // update the hints.
            if (force==false)
                return;
        }
    // set the new rotation parameters
    lastMat[0]=rotationMatrix[2][0];
    lastMat[1]=rotationMatrix[2][1];
    lastMat[2]=rotationMatrix[2][2];


    int i=-1,j=-1;// initialized to a trivial value to force memory assignment
    int plane ;
    int index;
    int *xIndex,*yIndex, *zIndex;
    int xMax;
    int yMax;
    int directionX;
    int directionY;


    // test the rotation matrix X coordiinates to define the maximum cubes width
    // then set the direction of the walk through the cubes
    if (rotationMatrix[0][0]!=0){
        xMax=width;
        directionX=rotationMatrix[0][0];
        xIndex=&i;

    }
    else if (rotationMatrix[0][1]!=0){
        xMax=height;
        directionX=rotationMatrix[0][1];
        yIndex=&i;
    }
    else if (rotationMatrix[0][2]!=0){
        xMax=depth;
        directionX=rotationMatrix[0][2];
        zIndex=&i;
    }
    // test the rotation matrix Y coordinates to define the maximum cubes height
    // and set the direction of the walk through the cubes
    if (rotationMatrix[1][0]!=0){
        yMax=width;
        directionY=rotationMatrix[1][0];
        xIndex=&j;
    }
    else if (rotationMatrix[1][1]!=0){
        yMax=height;
        directionY=rotationMatrix[1][1];
        yIndex=&j;
    }
    else if (rotationMatrix[1][2]!=0){
        yMax=depth;
        directionY=rotationMatrix[1][2];
        zIndex=&j;
    }

    // check the state of the Z axis on the rotation matrix to define the plane
    // that is being seen.
	if (rotationMatrix[2][2]!=0){	//if  the Z axis is aligned to Z in the scenario
		// use cursor Z position to get current plane
		plane = cursor.get_z();
        zIndex=&plane;
    }
    else if (rotationMatrix[2][1]!=0){
        // use cursor Y position  to get current plane
	    plane = cursor.get_y();
	    yIndex=&plane;
    }
    else if (rotationMatrix[2][0]!=0){
        // use cursor X position to get current plane
		 plane = cursor.get_x();
		 xIndex=&plane;
    }
    hList.clear();

    int hintNum=0;
    // set the new size of the horizontal hint list
    hList.resize(yMax);
    // walk through the selected plane and define the hints
    for (j=0;j<yMax;j++){
        index=j;  // used to walk in a positive or negative direction
        if (directionY<0){
            index=yMax-j-1;
        }
        hintNum=0;
        // create new row for number counting
        for ( i=0;i<xMax;i++){
            VirtualCube *curCube;
            curCube=cubesData[*xIndex][*yIndex][*zIndex];

            if (curCube->type ==TYPE_USED){
                hintNum++;
            }
            else {
                if (hintNum!=0){
                    // make cube hints adding numbers to the list row
                    if (directionX<0) hList[index].push_back(hintNum);
                    else hList[index].push_front(hintNum);
                    hintNum=0;
                }
            }
        }
        if (hintNum!=0){
            if (directionX<0) hList[index].push_back(hintNum);
                    else hList[index].push_front(hintNum);
            hintNum=0;
        }
        // if there is no number, put the default 0
        if (hList[index].size()==0){
            hList[index].push_back(0);
        }
    }


    // create the VERTICAL list of numbers
    vList.clear();
    vList.resize(xMax);
     hintNum=0;
    for ( i=0;i<xMax;i++){
        index=i;  // used to walk in a positive or negative direction
        if (directionX<0){
            index=xMax-i-1;
        }

        for (j=0;j<yMax;j++){
            VirtualCube *curCube;
              curCube=cubesData[*xIndex][*yIndex][*zIndex];
             if (curCube->type ==TYPE_USED){
                hintNum++;
             }
             else {
                 if (hintNum!=0){
                    if (directionY>0) vList[index].push_back(hintNum);
                    else vList[index].push_front(hintNum);
                    hintNum=0;
                }
            }
        }
        if (hintNum!=0){
            if (directionY>0) vList[index].push_back(hintNum);
                    else vList[index].push_front(hintNum);
            hintNum=0;
        }
        // if there is no number, put the default 0
        if (vList[index].size()==0){
            vList[index].push_back(0);
        }

    }
    Scenario3D &scenario3d = Scenario3D::get_handle();
    scenario3d.update_hints(true);


}
char *Scenario::get_hint_row(int row)
{
	char *string = new char[80];
	char buff[30];
	buff[0]='\0';
	string[0]='\0';
	for (int i=0;i<hList[row].size();i++){
			sprintf(buff,"%d ",hList[row].at(i));
			strncat(string, buff, 79);
	}

	return string;
}
char *Scenario::get_hint_col(int col)
{
	char *string = new char[80];
	char buff[30];
	buff[0]='\0';
	string[0]='\0';
	for (int j=vList[col].size()-1;j>=0;j--){
		sprintf(buff,"%d ",vList[col].at(j));
			strncat(string, buff, 79);
	}

	return string;
}
int Scenario::get_max_hint_row(){
	return hList.size();
}
int Scenario::get_max_hint_col(){
		return vList.size();
}

bool Scenario::show_cube_type(){
    return showCubeType;
}
void Scenario::show_cube_type(bool state){
    showCubeType=state;
}
