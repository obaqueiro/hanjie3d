/////////////////////////////////////////////////////////////////////////////
// Name:        frmjoyconfig.cpp
// Purpose:     
// Author:      Omar Baqueiro Espinosa
// Modified by: 
// Created:     11/02/05 19:43:23
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

#if defined(__GNUG__) && !defined(__APPLE__)
#pragma implementation "frmjoyconfig.h"
#endif

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "frmjoyconfig.h"

////@begin XPM images
////@end XPM images

/*!
 * Application instance implementation
 */

////@begin implement app
IMPLEMENT_APP( FrmJoyConfigApp )
////@end implement app

/*!
 * FrmJoyConfigApp type definition
 */

IMPLEMENT_CLASS( FrmJoyConfigApp, wxApp )

/*!
 * FrmJoyConfigApp event table definition
 */

BEGIN_EVENT_TABLE( FrmJoyConfigApp, wxApp )

////@begin FrmJoyConfigApp event table entries
////@end FrmJoyConfigApp event table entries

END_EVENT_TABLE()

/*!
 * Constructor for FrmJoyConfigApp
 */

FrmJoyConfigApp::FrmJoyConfigApp()
{
////@begin FrmJoyConfigApp member initialisation
////@end FrmJoyConfigApp member initialisation
}

/*!
 * Initialisation for FrmJoyConfigApp
 */

bool FrmJoyConfigApp::OnInit()
{    
////@begin FrmJoyConfigApp initialisation
    // Remove the comment markers above and below this block
    // to make permanent changes to the code.

#if wxUSE_XPM
    wxImage::AddHandler( new wxXPMHandler );
#endif
#if wxUSE_LIBPNG
    wxImage::AddHandler( new wxPNGHandler );
#endif
#if wxUSE_LIBJPEG
    wxImage::AddHandler( new wxJPEGHandler );
#endif
#if wxUSE_GIF
    wxImage::AddHandler( new wxGIFHandler );
#endif
    MyDialog* mainWindow = new MyDialog(NULL, ID_DIALOG, _("Hanjie^3 Joystick Setup"));
    mainWindow->Show(true);
////@end FrmJoyConfigApp initialisation

    return true;
}

/*!
 * Cleanup for FrmJoyConfigApp
 */
int FrmJoyConfigApp::OnExit()
{    
////@begin FrmJoyConfigApp cleanup
    return wxApp::OnExit();
////@end FrmJoyConfigApp cleanup
}

