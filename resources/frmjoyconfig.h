/////////////////////////////////////////////////////////////////////////////
// Name:        frmjoyconfig.h
// Purpose:     
// Author:      Omar Baqueiro Espinosa
// Modified by: 
// Created:     11/02/05 19:43:23
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

#ifndef _FRMJOYCONFIG_H_
#define _FRMJOYCONFIG_H_

#if defined(__GNUG__) && !defined(__APPLE__)
#pragma interface "frmjoyconfig.cpp"
#endif

/*!
 * Includes
 */

////@begin includes
#include "wx/image.h"
////@end includes

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
////@end control identifiers

/*!
 * FrmJoyConfigApp class declaration
 */

class FrmJoyConfigApp: public wxApp
{    
    DECLARE_CLASS( FrmJoyConfigApp )
    DECLARE_EVENT_TABLE()

public:
    /// Constructor
    FrmJoyConfigApp();

    /// Initialises the application
    virtual bool OnInit();

    /// Called on exit
    virtual int OnExit();

////@begin FrmJoyConfigApp event handler declarations

////@end FrmJoyConfigApp event handler declarations

////@begin FrmJoyConfigApp member function declarations

////@end FrmJoyConfigApp member function declarations

////@begin FrmJoyConfigApp member variables
////@end FrmJoyConfigApp member variables
};

/*!
 * Application instance declaration 
 */

////@begin declare app
DECLARE_APP(FrmJoyConfigApp)
////@end declare app

#endif
    // _FRMJOYCONFIG_H_
