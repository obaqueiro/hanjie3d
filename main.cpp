/*
 Part of Hanjie3D project
 
 Released under the GPL 2.0 License (See file gpl-2.0.txt or http://www.gnu.org/licenses/gpl-2.0.txt)
 Copyright (2006 - 2011)
 Author: Omar Baqueiro Epinosa

*/
#include <cstdlib>
#include <iostream>
#include <string>
#include "main.h"
#include "scenario.h"
#include "graphicengine.h"
#include "camera.h"
#include "inputengine.h"
#include <SDL/SDL.H>
using namespace std;

bool parse_args(int argc, char *argv[]);

int main(int argc, char *argv[])
{
	Scenario &scenario = Scenario::get_handle()	;
	GraphicEngine &graphics= GraphicEngine::get_handle();
	Camera &camera = Camera::get_handle();
	InputEngine &input= InputEngine::get_handle();
	float lastFPS;


    bool fullScreen=parse_args(argc, argv);

   	// initialize SDL ystem
	if (SDL_Init (SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) < 0)	{
		fprintf(stderr, "Video initilization failed %s\n",
			SDL_GetError());
			return -1;
	}

	bool done = false;
	graphics.init_graphics(fullScreen);
    input.init();
	scenario.load_scenario("silla.han");

	// main game loop
	while (!done){
		SDL_Event event;
		while (SDL_PollEvent(&event)){

			switch (event.type){

				case SDL_QUIT:
					done = true;
					break;
				case SDL_KEYDOWN:
				case SDL_KEYUP:
				case SDL_MOUSEMOTION:

				case SDL_MOUSEBUTTONDOWN:
				case SDL_MOUSEBUTTONUP:
				case SDL_JOYAXISMOTION:
				case SDL_JOYBALLMOTION:
				case SDL_JOYHATMOTION:
				case SDL_JOYBUTTONDOWN:
				case SDL_JOYBUTTONUP:
					// if the event is an input event, handle it via InputEngine
					done =input.handle_event(event);
					break;
				default:
					break;
			}
		}

		input.update();
			scenario.update();
		scenario.get_cursor().update();
		camera.update();

		graphics.draw_scene();
		float FPS = graphics.get_FPS();
		if (FPS!=lastFPS){

            char caption[80];
            sprintf(caption,"H^3 - by Omar Baqueiro. FPS=%f",FPS);
            SDL_WM_SetCaption (caption,"images/snow.ico");
		}


	}


    return EXIT_SUCCESS;
}

bool parse_args(int argc, char *argv[]){
    bool full=false;
    Scenario &scenario = Scenario::get_handle();
    if (argc>1){
        for (int i=1;i<argc;i++){
            if (strcmp(argv[i],"-cheat1")==0){
                scenario.show_cube_type(true);
            }
            if (strcmp(argv[i],"-full")==0){
                full=true;
            }


        }
    }
    return full;
}
