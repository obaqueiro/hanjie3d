/*
 Part of Hanjie3D project
 
 Released under the GPL 2.0 License (See file gpl-2.0.txt or http://www.gnu.org/licenses/gpl-2.0.txt)
 Copyright (2006 - 2011)
 Author: Omar Baqueiro Epinosa

*/

// Scenario class
// Contains the scenario setup information with the map of the cube model
// Singleton class

#ifndef SCENARIO_H
#define SCENARIO_H
#include <stdio.h>

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <deque>


#include "cursor.h"


#define  FILENAME_USED_CUBE_SURFACE   "images/texture1.bmp"
#define FILENAME_UNUSED_CUBE_SURFACE  "images/texture4.bmp"

enum CubeState{
	STATE_NORMAL=0,  // a cube that is not selected as X or O
	STATE_X_CUBE,    // a cube that is selected as X (do not belong to 3D Model)
	STATE_O_CUBE	    // a cube that is selecte as O (it belongs to 3D Model)
};
// The type of cube
enum CubeType{
	TYPE_UNUSED=0, 	// Cube used by the 3D Model
	TYPE_USED=1     // Cube Unused by the 3D Model
};

// Contains all the information that define a cube in the scenario
struct VirtualCube{
	CubeState state;
	CubeType type;
};

enum Axis {
	AXIS_X,
	AXIS_Y,
	AXIS_Z
};
struct Rotation{
	int angle;
	Axis axis;
};

/*
 * Contains all the information of the current scenario.
 */
class Scenario
{

	float static const CUBE_SIZE = 1; // size of each cube in the scenario
	float static const CUBE_SPACE =0; // space between each cube in scenario
	// radians to be rotated each frame when a rotation operation is done
	int static const ROTATION_STEP=3;

	float centerX, centerY, centerZ; // position of the center of the cube
	int rotx, roty, rotz; // rotation of the cube
	int deltaX, deltaY, deltaZ; // used to accumulate rotation
	float cubeSpace; // specifies the space between cubes

	// The current selected cube cursor information

	Cursor cursor;
	// private constructor
	Scenario();
	// number of cubes on width
	int width;
	// number of cubes on height
	int height;
	// number of cubes on depth
	int depth;

    //  Flag set at by the -cheat1 program parameter code
    bool showCubeType;

	// A 3D MxNxL array with the information of the cubes
	VirtualCube *** *cubesData;

	// horizontal hint number list
	std::deque<std::deque<int> > hList;
	// vertical hint number list
	std::deque<std::deque<int> > vList;

	// Rotate the scenario around X and Y  SCREEN axis
	void Rotate();

	// swap the values of axis1 and axis2 and multiply the axis2 values by -1
	// help function to manipualte the rotationMatrix rotation.
    void swap_axis(int axis1[3], int axis2[3]);

	public:
		char *get_hint_row(int row);
		char *get_hint_col(int col);
		int get_max_hint_row();
		int get_max_hint_col();
		// A 3x3 matrix representing the [x y z] direction of the scenario axis
		// used to get the current axis setup
		int rotationMatrix[3][3];

		float ambientLight[4];
	    float lightPosition[4];

		// returns object instance
		static Scenario &get_handle();


		// class destructor
		~Scenario();
		// load scenario from a file return TRUE if scenario was loaded succesfully
		bool load_scenario(char *filename);

		// print the cubesType data to stdout
		void print_cubes_data();

		// prints the current rotation matrix
		void print_rotation_matrix();

		// Update the hint numbers to display above the cubes
		void update_hints(bool force);

		// update rotation values of the scenario each frame
		void update_rotation(void);

		// rotate cube +-90 degrees over the X axis
		void rotate_x(int direction);
		// void rotate the cube +-90 degrees over the Y axis
		void rotate_y(int direction);
		// void rotate the cube +-90 degrees over the Z axis
		void rotate_z(int direction);

		// modify the number of spaces between each cube
		void increment_cube_space();
		void decrement_cube_space();

		int get_width(); // returns the number of cubes on X axis
		int get_height();// returns the number of cubes on Y axis
		int get_depth();// returns the number of cubes on Z axis

		int get_rotx(); // returns  the degrees of rotation on X axis
		int get_roty();// returns  the degrees of rotation on Y axis
		int get_rotz();// returns  the degrees of rotation on Z axis

		float get_centerx(); // returns the center of the scenario on X axis
		float get_centery(); // returns the center of the scenario on Y axis
		float get_centerz(); // returns the center of the scenario on Z axis

		float get_cubesize(); // returns the size of each cube
		float get_cubespace(); // returns the space between each cube

		// returns the data of the specified cube in the scenario
		VirtualCube & get_cube(int x, int y, int z);
		// returns a the hadler to the scenario cursor;
		Cursor &get_cursor();

		// set the selected cube state from X to O to NORMAL
		void toggle_cubestate();
		// set the selected cube state
		void set_cubestate(CubeState newState);
		bool show_cube_type();
		void show_cube_type(bool state);

		// Updates the state of the scenario, must be called each frame
		void update();
};


#endif // SCENARIO_H
