/*
 Part of Hanjie3D project
 
 Released under the GPL 2.0 License (See file gpl-2.0.txt or http://www.gnu.org/licenses/gpl-2.0.txt)
 Copyright (2006 - 2011)
 Author: Omar Baqueiro Epinosa

*/

#include "inputengine.h" // class's header file

// class constructor
InputEngine::InputEngine()
{

}
void InputEngine::init()
{
 int i = SDL_NumJoysticks();
 if (i>0){
  SDL_JoystickEventState(SDL_ENABLE);
  joystick=SDL_JoystickOpen(0);
   print_joystick_info();
 }
}
// class destructor
InputEngine::~InputEngine()
{
}


InputEngine &InputEngine::get_handle()
{
	static InputEngine input;
	return input;
}

bool InputEngine::handle_event(SDL_Event event)
{
	Scenario &scenario = Scenario::get_handle();
	Camera &camera = Camera::get_handle();
	char text[100];
	Uint8* keys;

	keys = SDL_GetKeyState(NULL);

	if (keys[SDLK_LSHIFT] || keys[SDLK_LALT] || keys[SDLK_LCTRL]){
		return false;
	}
	switch (event.type)	{
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym) {
				case SDLK_ESCAPE:
					return true; // end game
					break;
				case SDLK_KP8:
				case SDLK_UP:
					scenario.get_cursor().move_up(1);
					break;
				case SDLK_KP5:
				case SDLK_DOWN:
					scenario.get_cursor().move_down(1);
					break;
				case SDLK_KP4:
				case SDLK_LEFT:
					scenario.get_cursor().move_left(1);
					break;
				case SDLK_KP6:
				case SDLK_RIGHT:
					scenario.get_cursor().move_right(1);
					break;

				case SDLK_KP9:
				case SDLK_e:
					scenario.get_cursor().move_forward(1);
					break;

				case SDLK_KP7:
				case SDLK_q:
					scenario.get_cursor().move_backward(1);
					break;

				case SDLK_SPACE:
					scenario.toggle_cubestate();
					break;

				case SDLK_KP0:
				case SDLK_c:
					scenario.set_cubestate(STATE_O_CUBE);
					break;
				case SDLK_KP_PERIOD:
				case SDLK_x:
					scenario.set_cubestate(STATE_X_CUBE);
					break;

				case SDLK_KP_ENTER:
				case SDLK_z:
					scenario.set_cubestate(STATE_NORMAL);
					break;

				case SDLK_END:
					scenario.increment_cube_space();
					break;
				case SDLK_HOME:
					scenario.decrement_cube_space();
					break;


				default:
					break;
			}

		case SDL_KEYUP:
				break;
		case SDL_MOUSEMOTION:
			// if the left button is pressed
			if (event.motion.state & SDL_BUTTON(1)){
				if (event.motion.xrel>MOUSE_THRESHOLD_X){
					// rotate on X in a negative direction
					scenario.rotate_y(1);
				}
				if (event.motion.xrel<-MOUSE_THRESHOLD_X){
					// rotate on X in a positive direction
					scenario.rotate_y(-1);
				}
					if (event.motion.yrel>MOUSE_THRESHOLD_Y){
					// rotate on Y in a negative direction
					scenario.rotate_x(1);
				}
				if (event.motion.yrel<-MOUSE_THRESHOLD_Y){
					// rotate on X in a negative direction
					scenario.rotate_x(-1);
				}
			}
			break;
		case SDL_MOUSEBUTTONDOWN:
			switch (event.button.button){
				case 4:  // scroll wheel UP
					camera.zpos+=MOUSE_WHEEL_SENSITIVITY;
					break;
				case 5: // scroll wheel DOWN
					camera.zpos-=MOUSE_WHEEL_SENSITIVITY;
					break;
			}

			break;
		case SDL_MOUSEBUTTONUP:

			break;
		case SDL_JOYAXISMOTION:

			break;

		case SDL_JOYBALLMOTION:


			  break;
		case SDL_JOYHATMOTION:
			  break;
		case SDL_JOYBUTTONDOWN:
			  break;
		case SDL_JOYBUTTONUP:
			  break;
			break;
		default:
			break;
	}
	return false;
}

void InputEngine::update()
{
	Camera &camera = Camera::get_handle();
	Scenario &scenario = Scenario::get_handle()	;

	Uint8* keys;

	keys = SDL_GetKeyState(NULL);
	if (keys[SDLK_UP]){
		if (keys[SDLK_LSHIFT] ){
			camera.zpos+=0.1;
		}
		else
		if (keys[SDLK_LALT]){
			// rotate on X in a negative direction
			scenario.rotate_x(-1);
		}
		else if( keys[SDLK_LCTRL]){
			camera.ypos -= .1;
		}

	}
	if (keys[SDLK_DOWN]){
			if (keys[SDLK_LSHIFT]){
				camera.zpos-=0.1;
			}
			else if (keys[SDLK_LALT]){
			// rotate on X in a positive direction
			scenario.rotate_x(1);
			}
			else if( keys[SDLK_LCTRL]){
				camera.ypos += .1;
			}

	}


	if ( keys[SDLK_LEFT] ) {
			if (keys[SDLK_LALT]){
				scenario.rotate_y(-1);
			}
			else if( keys[SDLK_LCTRL]){
				camera.xpos += .1;
			}

	}
	if ( keys[SDLK_RIGHT] ) {
			if (keys[SDLK_LALT]){
			scenario.rotate_y(1);
			}
			else if( keys[SDLK_LCTRL]){
					camera.xpos -= .1;
			}

	}
	if (keys[SDLK_END]){
		scenario.increment_cube_space();
	}

	if (keys[SDLK_HOME]){
		scenario.decrement_cube_space();
	}

}
void InputEngine::print_joystick_info()
{
	if (joystick!=NULL){
		printf("Joystick information: [%s]\n",
			SDL_JoystickName(SDL_JoystickIndex(joystick)));
		printf("Number of axes: %d\n",SDL_JoystickNumAxes(joystick));
		printf ("Number of balls: %d\n",SDL_JoystickNumBalls(joystick));
		printf("Number of hats: %d\n",SDL_JoystickNumHats(joystick));
		printf("Number of buttons: %d\n",SDL_JoystickNumButtons(joystick));

	}
}
