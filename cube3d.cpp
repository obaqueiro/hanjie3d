/*
 Part of Hanjie3D project
 
 Released under the GPL 2.0 License (See file gpl-2.0.txt or http://www.gnu.org/licenses/gpl-2.0.txt)
 Copyright (2006 - 2011)
 Author: Omar Baqueiro Epinosa

*/

#include "cube3d.h" // class's header file
#include <gl/gl.h>

// class constructor
Cube3D::Cube3D()
{
	const float tileValue=3.0f;
	// create the cube display list
	cubeHandler = glGenLists(1);

	 GLfloat v0[] = { -0.5f, -0.5f,  0.5f };
     GLfloat v1[] = {  0.5f, -0.5f,  0.5f };
     GLfloat v2[] = {  0.5f,  0.5f,  0.5f };
     GLfloat v3[] = { -0.5f,  0.5f,  0.5f };
     GLfloat v4[] = { -0.5f, -0.5f, -0.5f };
     GLfloat v5[] = {  0.5f, -0.5f, -0.5f };
     GLfloat v6[] = {  0.5f,  0.5f, -0.5f };
     GLfloat v7[] = { -0.5f,  0.5f, -0.5f };



	glNewList(cubeHandler, GL_COMPILE);
	 /* Send our triangle data to the pipeline. */
    glBegin( GL_TRIANGLES );

    // front face
    glTexCoord2f(0.0, 0.0); glVertex3fv( v0 ); // bottom left  [front face]
	glTexCoord2f(tileValue, 0.0); glVertex3fv( v1 ); // bottom right [front face]
    glTexCoord2f(tileValue, tileValue); glVertex3fv( v2 ); // top right [front face]
     glTexCoord2f(0.0, 0.0); glVertex3fv( v0 );
	glTexCoord2f(tileValue, tileValue); glVertex3fv( v2 );
    glTexCoord2f(0.0, tileValue); glVertex3fv( v3 ); // top left


    glTexCoord2f(0.0, 0.0); glVertex3fv( v1 );
    glTexCoord2f(tileValue, 0.0); glVertex3fv( v5 );
    glTexCoord2f(tileValue, tileValue); glVertex3fv( v6 );
    glTexCoord2f(0.0, 0.0); glVertex3fv( v1 );
	glTexCoord2f(tileValue, tileValue); glVertex3fv( v6 );
	glTexCoord2f(0.0, tileValue);  glVertex3fv( v2 );


    glTexCoord2f(0.0, 0.0); glVertex3fv( v5 );
    glTexCoord2f(tileValue, 0.0);glVertex3fv( v4 );
    glTexCoord2f(tileValue, tileValue);glVertex3fv( v7 );
    glTexCoord2f(0.0, 0.0);glVertex3fv( v5 );
	glTexCoord2f(tileValue, tileValue) ;glVertex3fv( v7 );
    glTexCoord2f(0.0, tileValue); glVertex3fv( v6 );


    glTexCoord2f(0.0, 0.0);glVertex3fv( v4 );
    glTexCoord2f(tileValue, 0.0);glVertex3fv( v0 );
    glTexCoord2f(tileValue, tileValue);glVertex3fv( v3 );
    glTexCoord2f(0.0, 0.0);glVertex3fv( v4 );
	 glTexCoord2f(tileValue, tileValue);glVertex3fv( v3 );
    glTexCoord2f(0.0, tileValue);glVertex3fv( v7 );


    glTexCoord2f(0.0, 0.0); glVertex3fv( v3 );
    glTexCoord2f(tileValue, 0.0);glVertex3fv( v2 );
    glTexCoord2f(tileValue, tileValue);glVertex3fv( v6 );
    glTexCoord2f(0.0, 0.0);glVertex3fv( v3 );
	glTexCoord2f(tileValue, tileValue);glVertex3fv( v6 );
   	glTexCoord2f(0.0, tileValue); glVertex3fv( v7 );


   	glTexCoord2f(0.0, 0.0); glVertex3fv( v1 );
    glTexCoord2f(tileValue, 0.0); glVertex3fv( v0 );
    glTexCoord2f(tileValue, tileValue); glVertex3fv( v4 );
    glTexCoord2f(0.0, 0.0);glVertex3fv( v1 );
	glTexCoord2f(tileValue, tileValue);glVertex3fv( v4 );
   	glTexCoord2f(0.0, tileValue);glVertex3fv( v5 );

    glEnd( );

	glEndList();

}

// class destructor
Cube3D::~Cube3D()
{
	// insert your code here
}

void Cube3D::draw()
{
	glCallList(cubeHandler);
}
