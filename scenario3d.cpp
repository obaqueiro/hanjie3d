/*
 Part of Hanjie3D project
 
 Released under the GPL 2.0 License (See file gpl-2.0.txt or http://www.gnu.org/licenses/gpl-2.0.txt)
 Copyright (2006 - 2011)
 Author: Omar Baqueiro Epinosa

*/
#include "scenario3d.h" // class's header file

// class constructor
Scenario3D::Scenario3D()
{

	// setup rotation matrix to the identity matrix.
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glGetFloatv(GL_MODELVIEW_MATRIX,rotationMatrix);
    hintRow=NULL;
    hintCol=NULL;

    // create text hints polygon display list
    hintDisplayList = glGenLists(1);
     glNewList(hintDisplayList, GL_COMPILE);
		glBegin(GL_QUADS);
		/* Recall that the origin is in the lower-left corner
		   That is why the TexCoords specify different corners
		   than the Vertex coors seem to. */
        glTexCoord2f(0.0f, 1.0f);
            glVertex2f(0.0f, 0.0f);
		glTexCoord2f(1.0f, 1.0f);
            glVertex2f(1.0f, 0.0f);
		glTexCoord2f(1.0f, 0.0f);
            glVertex2f(1.0f, 1.0f);
        glTexCoord2f(0.0f, 0.0f);
		  	glVertex2f(0.0f, 1.0f);
		glEnd();

	 glEndList();
	// Load scenario textures to use in the different kinds of cubes
	load_textures();
}

// class destructor
Scenario3D::~Scenario3D()
{
	// insert your code here
}

void Scenario3D::rotate_x(int degrees)
{
	// update the rotation matrix
	glPushMatrix();				// save the current matrix
	glLoadMatrixf(rotationMatrix);   // load the rotation matrix to modify
	glRotatef(degrees,1.0f, 0.0f,0.0f); // modify rotation matrix with new coord.
	glGetFloatv(GL_MODELVIEW_MATRIX,rotationMatrix);  // save the modified matrix
	glPopMatrix();//  restore the matrix to its original value
}

void Scenario3D::rotate_y(int degrees)
{
	// update the rotation matrix
	glPushMatrix();				// save the current matrix
	glLoadMatrixf(rotationMatrix);   // load the rotation matrix to modify
	glRotatef(degrees,0.0f, 1.0f,0.0f); // modify rotation matrix with new coord.
	glGetFloatv(GL_MODELVIEW_MATRIX,rotationMatrix);  // save the modified matrix
	glPopMatrix();//  restore the matrix to its original value
}

void Scenario3D::rotate_z(int degrees)
{
	// update the rotation matrix
	glPushMatrix();				// save the current matrix
	glLoadMatrixf(rotationMatrix);   // load the rotation matrix to modify
	glRotatef(degrees,0.0f,0.0f,1.0f); // modify rotation matrix with new coord.
	glGetFloatv(GL_MODELVIEW_MATRIX,rotationMatrix);  // save the modified matrix
	glPopMatrix();//  restore the matrix to its original value
}
void Scenario3D::draw()
{
	Scenario &scenario = Scenario::get_handle();

	float rotx = (float) scenario.get_rotx();
	float roty = (float) scenario.get_roty();
	float rotz = (float) scenario.get_rotz();
	float centerX = scenario.get_centerx();
	float centerY = scenario.get_centery();
	float centerZ = scenario.get_centerz();
	float cubeSize = scenario.get_cubesize();
	float cubeSpace = scenario.get_cubespace();
	float width = scenario.get_width();
	float height = scenario.get_height();
	float depth = scenario.get_depth();

    float transX, transY, transZ;


	Cursor &cursor = scenario.get_cursor();
	// create a new cube
	Cube3D cube1;


	// render background image
	draw_background();
	// rotate the scenario
	glMultMatrixf(rotationMatrix);

	// draw axis
	/*glBegin(GL_LINES);
		glColor3f(1,0,0);
		glVertex3f(0,0,0);
		glVertex3f(10,0,0);
		glColor3f(0,1,0);
		glVertex3f(0,0,0);
		glVertex3f(0,10,0);
		glColor3f(0,0,1);
		glVertex3f(0,0,0);
		glVertex3f(0,0,10);
	glEnd();*/


	// the center of the cube
	/*glPointSize(3.0f);
	glBegin(GL_POINTS);
		glVertex3f(0,0,0);
	glEnd();*/



	/* set the coordinates to start drawing the cube depeding on the orientation
	   of the scenario, this is done to draw the cubes from the back to the front
	   to impelement transparency
	   */
	transX=((scenario.rotationMatrix[2][0]==1)?(centerX-cubeSize/2):(-centerX+cubeSize/2));
	transY=((scenario.rotationMatrix[2][1]==1)?(centerY-cubeSize/2):(-centerY+cubeSize/2));
	transZ=((scenario.rotationMatrix[2][2]==1)?(centerZ-cubeSize/2):(-centerZ+cubeSize/2));
	glTranslatef(transX,transY,transZ);

	GraphicEngine::enable_texture(this->surfUsedCube);

	VirtualCube cube;
	float red=1,green=1,blue=1,alpha;

   // draw all the cubes in the scenario
	for (int i=0;i<width;i++){
		int indexI=i;

		// Set the appropiate increment to draw the cubes from back to the front
		if (scenario.rotationMatrix[2][0]==1){
			indexI=(int)width-1-i;
		}

		glPushMatrix();

		for (int j=0;j<height;j++){
			int indexJ=j;
			// Set the appropiate increment to draw the cubes from back to the front
			if (scenario.rotationMatrix[2][1]==1){
				indexJ=(int)height-1-j;
			}

			glPushMatrix();
			for (int k=0;k<depth;k++){
				int indexK=k;
				// Set the appropiate increment to draw the cubes from back to the front
				if (scenario.rotationMatrix[2][2]==1){
					indexK=(int)depth-1-k;
				}
				cube = scenario.get_cube(indexI,indexJ,indexK);
				alpha=1;

				// select main texture to draw the cube
				GraphicEngine::enable_texture(this->surfUsedCube);

				// show or hide the cube according to its state
				switch (cube.state){
					case STATE_NORMAL:
						// if the cube state has not been specified change texture
						GraphicEngine::enable_texture(this->surfUnusedCube);
						alpha = .6;
						break;
					case STATE_O_CUBE:
						// if the cube is used, put different texture
						alpha =1;
						break;
					case STATE_X_CUBE:
						alpha =.05;
						break;
				}

				if (scenario.show_cube_type()==true && cube.type==TYPE_USED)	{
                   green=0;
                   blue=0;
                }
                else{
                     green=blue=1;
                }
					glColor4f(red, green, blue,alpha);

				// draw the cursor on the corresponding cube
				if (cursor.get_x() == indexI && cursor.get_y()==indexJ
					 && cursor.get_z() == indexK){
						float sf;
						// set an "orangish" color for the cursor
						glColor4f(9.970f, 0.260f, 0.027f,alpha*5);
						sf=cursor.get_scale_factor();
						glPushMatrix();
						glScalef(sf,sf,sf);
						cube1.draw();
						glPopMatrix();
						// draw the hint numbers
                        // go to the init of the cube drawing
                        draw_hints();

				}
				else
				{
					cube1.draw();
				}

				float translation=cubeSpace+cubeSize;
				if (scenario.rotationMatrix[2][2]==1   ){
					translation=-translation;
				}
				glTranslatef(0.0f,0.0f,translation);

			}
			glPopMatrix();
			float translation=cubeSpace+cubeSize;
			if (scenario.rotationMatrix[2][1]==1   ){
				translation=-translation;
			}
			glTranslatef(0.0f,translation,0.0f);

		}
		glPopMatrix();
		float translation=cubeSpace+cubeSize;

		if (scenario.rotationMatrix[2][0]==1){
			translation=-translation;
		}
		glTranslatef(translation,0.0f,0.0f);
	}
}

void Scenario3D::load_textures()
{

	SDL_Surface *surf;
	GLfloat 	texcoord[4];

	// create SDL surface from file
	surf = GraphicEngine::load_bitmap("images/usedcube.bmp");
	// generate OpenGL texture from surface
	this->surfUsedCube = GraphicEngine::load_texture(surf,texcoord);
	// free the surface and load the next BMP
	SDL_FreeSurface(surf);

	surf = GraphicEngine::load_bitmap("images/undefinedcube.bmp");
	// generate OpeGL textre from surface
	this->surfUnusedCube = GraphicEngine::load_texture(surf,texcoord);
	SDL_FreeSurface(surf);

	surf = GraphicEngine::load_bitmap("images/background.bmp");
	// generate OpeGL textre from surface
	this->surfBackground= GraphicEngine::load_texture(surf,texcoord);
	SDL_FreeSurface(surf);


}
void Scenario3D::draw_background()
{


	glPushMatrix();
	glLoadIdentity();
	 // Switch to orthographic view for background drawing



	glColor4f(.5,.5,.5,.4);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0.0f, 1.0f, 0.0f, 1.0f);

    glMatrixMode(GL_MODELVIEW);

    glBindTexture(GL_TEXTURE_2D, surfBackground);    // Background texture

    // No depth buffer writes for background

	glDepthMask(GL_FALSE);
    // Background image
    glBegin(GL_QUADS);

        glTexCoord2f(0.0f, 0.0f);

        glVertex2f(0.0f, 0.0f);



        glTexCoord2f(10.0, 0.0f);

        glVertex2f(1.0f, 0.0f);



        glTexCoord2f(10.0, 10.0);

        glVertex2f(1.0f, 1.0f);



        glTexCoord2f(0.0f, 10.0);

        glVertex2f(0.0f, 1.0f);

    glEnd();



    // Back to 3D land

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();

    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
  	glDepthMask(GL_TRUE);
}

void Scenario3D::draw_hints()
{

	Scenario &scenario = Scenario::get_handle();
    // Switch to orthographic view for background drawing
    glPushMatrix();
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0.0f, 10.0f, 0.0f, 10.0f);

    glMatrixMode(GL_MODELVIEW);
    //glTranslatef(-1.5f, 0,0.0f);

    glPushMatrix();
	glColor4f(1.0f, 0.0f, 0.0f,1.0f);

	// draw horizontal hints
	for (int i=0;i<scenario.get_max_hint_row();i++){
		glBindTexture(GL_TEXTURE_2D, hintRow[i]);
 		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glCallList(hintDisplayList);
        glTranslatef(0.0f, 1.0f,0.0f);

	}
	glPopMatrix();
// draw horizontal hints
    glTranslatef(0.0f,9.0f,0.0f);

	for (int i=0;i<scenario.get_max_hint_col();i++){
		glBindTexture(GL_TEXTURE_2D, hintCol[i]);
 		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glCallList(hintDisplayList);
        glTranslatef(1.0f, 0.0f,0.0f);

	}
   // Back to 3D land
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

	return;
}
void Scenario3D::update_hints(bool force)
{
    static int lastMat[3]={-1,-1,-1}; // the last Z rotation matrix
    Scenario &scenario = Scenario::get_handle();
    // check if the rotation has been modified
    if (lastMat[0]==scenario.rotationMatrix[2][0] &&
        lastMat[1]==scenario.rotationMatrix[2][1] &&
        lastMat[2]==scenario.rotationMatrix[2][2] )
        {
            // if the rotation has not been modified, there is no need to
            // update the hints.
            if (force==false)
                return;
        }
    // set the new rotation parameters
    lastMat[0]=scenario.rotationMatrix[2][0];
    lastMat[1]=scenario.rotationMatrix[2][1];
    lastMat[2]=scenario.rotationMatrix[2][2];


    // delete current hint textures if available
    if (hintCol!=NULL){
            glDeleteTextures(hintColMax,hintCol);
            delete [] hintCol;
            hintCol=NULL;
    }
    if (hintRow!=NULL){
        glDeleteTextures(hintRowMax,hintRow);
        delete [] hintRow;
        hintRow=NULL;
    }

    // generate new textures
    hintColMax=scenario.get_max_hint_col();
    hintCol = new GLuint[hintColMax];
    glGenTextures(hintColMax,hintCol);

    hintRowMax=scenario.get_max_hint_row();
    hintRow= new GLuint[hintRowMax];
    glGenTextures(hintRowMax,hintRow);

	// draw horizontal hints
	for (int i=0;i<scenario.get_max_hint_row();i++){
		char *hint=scenario.get_hint_row(i);
		GraphicEngine::get_handle().generate_GL_text(hint,hintRow[i]);

		glFinish();
		delete hint;
	}
	for (int i=0;i<scenario.get_max_hint_col();i++){
	    char *hint = scenario.get_hint_col(i);
	    GraphicEngine::get_handle().generate_GL_text(hint,hintCol[i]);
	    delete hint;
	}

	return;
}
