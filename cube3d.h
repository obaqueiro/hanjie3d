/*
 Part of Hanjie3D project
 
 Released under the GPL 2.0 License (See file gpl-2.0.txt or http://www.gnu.org/licenses/gpl-2.0.txt)
 Copyright (2006 - 2011)
 Author: Omar Baqueiro Epinosa

*/
#ifndef CUBE_H
#define CUBE_H

/*
 * A graphical cube distplayed with display lists
 */
class Cube3D
{
	// the OpenGL compiled list handler id 
	int cubeHandler;
	public:
		// class constructor
		Cube3D();
		// class destructor
		~Cube3D();
		
		// draw the cube
		void draw();
};

#endif // CUBE_H
