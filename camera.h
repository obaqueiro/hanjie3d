/*
 Part of Hanjie3D project
 
 Released under the GPL 2.0 License (See file gpl-2.0.txt or http://www.gnu.org/licenses/gpl-2.0.txt)
 Copyright (2006 - 2011)
 Author: Omar Baqueiro Epinosa

*/


// Definition of the CAMERA class to manage the camera views of the program.

#ifndef CAMERA_H
#define CAMERA_H

/* 
 * Definition of the CAMERA class to manage the camera views of the program.
 */
class Camera
{
	private:
	// class constructor
		Camera();
		Camera(Camera const&){}    // copy ctor hidden
		Camera& operator=(Camera const&){return get_handle();}  // assign op hidden

	public:
		float xpos;
		float ypos;
		float zpos;
		float yrot;
		float xrot;

static Camera & Camera::get_handle(){
			static Camera camera ;
			return camera;
		}
		// class destructor
		~Camera();

		void update();
};

#endif // CAMERA_H
