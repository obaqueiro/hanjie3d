/*
 Part of Hanjie3D project
 
 Released under the GPL 2.0 License (See file gpl-2.0.txt or http://www.gnu.org/licenses/gpl-2.0.txt)
 Copyright (2006 - 2011)
 Author: Omar Baqueiro Epinosa

*/
#ifndef CURSOR_H
#define CURSOR_H



/*
 * Class Cursor
 * The scenario cursor 
 */
class Cursor
{
	private:
	int x;		 
	int y; 
	int z;
	float scaleFactor;
	int scaleDir;
	public:
		// class constructor
		Cursor();
		// class destructor
		~Cursor();
		
		void move_right(int units);
		void  move_up(int units);
		void  move_down(int units);
		void  move_left(int units);
		void  move_forward(int units);
		void  move_backward(int units);		
		
		int get_x(){
			return x;
		}		
		int get_y() {
			return y;
		}
		int get_z(){
			return z;
		}
		void set_x(int newX);
		void set_y(int newY);
		void set_z(int newZ);
		float get_scale_factor();
		void update();
};

#endif // CURSOR_H
