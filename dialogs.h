/*
 Part of Hanjie3D project
 
 Released under the GPL 2.0 License (See file gpl-2.0.txt or http://www.gnu.org/licenses/gpl-2.0.txt)
 Copyright (2006 - 2011)
 Author: Omar Baqueiro Epinosa

*/

/////////////////////////////////////////////////////////////////////////////
// Name:        dialogs.h
// Purpose:     
// Author:      
// Modified by: 
// Created:     11/02/05 18:23:02
// RCS-ID:      
// Copyright:   
// Licence:     

/////////////////////////////////////////////////////////////////////////////

#ifndef _DIALOGS_H_
#define _DIALOGS_H_

#if defined(__GNUG__) && !defined(__APPLE__)
#pragma interface "dialogs.cpp"
#endif

/*!
 * Includes
 */

////@begin includes
#include "wx/image.h"
////@end includes

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
////@end control identifiers

/*!
 * DialogsApp class declaration
 */

class DialogsApp: public wxApp
{    
    DECLARE_CLASS( DialogsApp )
    DECLARE_EVENT_TABLE()

public:
    /// Constructor
    DialogsApp();

    /// Initialises the application
    virtual bool OnInit();

    /// Called on exit
    virtual int OnExit();

////@begin DialogsApp event handler declarations
////@end DialogsApp event handler declarations

////@begin DialogsApp member function declarations
////@end DialogsApp member function declarations

////@begin DialogsApp member variables
////@end DialogsApp member variables
};

/*!
 * Application instance declaration 
 */

////@begin declare app
DECLARE_APP(DialogsApp)
////@end declare app

#endif
    // _DIALOGS_H_
