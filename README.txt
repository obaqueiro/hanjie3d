Hanjie3D
Omar Baqueiro Espinosa, 2005-2012. 
Released under the GPL 2.0 License.

A Proof of concept of a tridimensional Nonogram ( http://en.wikipedia.org/wiki/Nonogram ) game. 

The main idea is to set the cubes either as FILLED or EMPTY to form a figure based based on the provided numbers.
Similar to the Piccross game.

Version : 0.1 
 - Main engines running preliminarily.

=== Controls ===

  [ESC] Quit application

Movement:
  [UP] [KEYPAD8] Move cursor up
  [DOWN] [KEYPAD5] Move cursor down
  [LEFT] [KEYPAD4] Move cursor left
  [RIGHT] [KEYPAD6] Move cursor right
  [E] [KEYPAD9] Move cursor backward (away from screen)
  [Q] [KEYPAD7] Move cursor forward (towards screen)

Cube State change:
  [SPACE] Toogle cube space (EMPTY, FILLED, UNKNOWN[white])
  [KEYPAD0] Set cube state as FILLED
  [KEYPAD.] Set cube state EMPTY
  [KEYPAD ENTER] Set cube state UNKNOWN
  [END] Increase space among cubes 
  [HOME] Decrease space among cubes.




