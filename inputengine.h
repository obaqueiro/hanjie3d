/*
 Part of Hanjie3D project
 
 Released under the GPL 2.0 License (See file gpl-2.0.txt or http://www.gnu.org/licenses/gpl-2.0.txt)
 Copyright (2006 - 2011)
 Author: Omar Baqueiro Epinosa

*/

#ifndef INPUTENGINE_H
#define INPUTENGINE_H
#include <SDL/SDL.h>
#include "scenario.h"
#include "camera.h"

/*
 * Manages all the game input devices
 */
class InputEngine
{
	private:
        // THe input joystick.
        SDL_Joystick *joystick;
	// class constructors
		InputEngine();
		InputEngine(InputEngine const&){}    // copy ctor hidden
		InputEngine& operator=(InputEngine const&){}  // assign op hidden
	public:
           void init();
		static const int MOUSE_THRESHOLD_X=20;
		static const int MOUSE_THRESHOLD_Y=20;
		static const float MOUSE_WHEEL_SENSITIVITY=0.5;

		// class destructor
		~InputEngine();
		static InputEngine & get_handle();

		/* handle the specified input event updating the game returns true
		   if the user asked to quit */
		bool handle_event(SDL_Event event);

		// Upates whatever input action needs to be updated, must be called each frame
		void update();

		void print_joystick_info();


};

#endif // INPUTENGINE_H
