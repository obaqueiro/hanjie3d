/*
 Part of Hanjie3D project
 
 Released under the GPL 2.0 License (See file gpl-2.0.txt or http://www.gnu.org/licenses/gpl-2.0.txt)
 Copyright (2006 - 2011)
 Author: Omar Baqueiro Epinosa

*/
#include "gui.h"
#include "main.h"
#include <wx/msgdlg.h>

#define WXUSINGDLL
// ============================================================================
// implementation
// ============================================================================

// ----------------------------------------------------------------------------
// the application class
// ----------------------------------------------------------------------------


// 'Main program' equivalent: the program execution "starts" here
bool MyApp::OnInit()
{
    // create the main application window
    MyFrame &frame = MyFrame::get_handle();

    wxString str1("�Desea correr el programa en pantalla completa?",*wxConvCurrent);
    wxString str2("Hanjie^3",*wxConvCurrent);

	int answer = wxMessageBox(str1,str2,wxYES_NO | wxCANCEL);

	if (answer==wxCANCEL){
		exit(EXIT_SUCCESS);
	}

	bool fullscreen=false;
    if (answer==wxYES){
		fullscreen=true;
	}

    // and show it (the frames, unlike simple controls, are not shown when
    // created initially)
    frame.Show(true);


	// run the sdl game thread
	gameThread.set_fullscreen(fullscreen);
	gameThread.Create();
	gameThread.Run();


    // success: wxApp::OnRun() will be called which will enter the main message
    // loop and the application will run. If we returned false here, the
    // application would exit immediately.
    return true;
}

int MyApp::OnExit()
{
	// remove the SDL game thread
	if (gameThread.IsAlive()){
		gameThread.Delete();
		gameThread.Kill();
	}
	Exit();

}
// event handlers
// ----------------------------------------------------------------------------
// main frame
// ----------------------------------------------------------------------------

void MyFrame::OnQuit(wxCommandEvent& WXUNUSED(event))
{

    // true is to force the frame to close
	Close(true);
    	// remove the SDL game thread
	//Hide();

}

void MyFrame::OnAbout(wxCommandEvent& WXUNUSED(event))
{
    wxString msg;
    msg.Printf( _T("Hanjie^3 by Omar Baqueiro\n")
                _T("Copyright 2005. All rights reserved\n")
                _T("Contact the author at: obaqueiro@gmail.com")

				);

    wxMessageBox(msg, _T("About H^3"), wxOK | wxICON_INFORMATION, this);
}
MyFrame & MyFrame::get_handle()
{
	static MyFrame frame(_T("H^3 Version 0.0.5"));
	return frame;
}
// frame constructor
MyFrame::MyFrame(const wxString& title)
       : wxFrame(NULL, wxID_ANY, title)
{
    // set the frame icon
    SetIcon(wxICON(sample));

#if wxUSE_MENUS
    // create a menu bar
    wxMenu *fileMenu = new wxMenu;

    // the "About" item should be in the help menu
    wxMenu *helpMenu = new wxMenu;
    wxString s;

    lblCursorPosition =
		new wxStaticText(this,Gui_cursorStatus,_T("Omar"),wxPoint(0,0),wxSize(100,20));
    txtMatrix =
	new wxTextCtrl(this,Gui_matrixText,s,wxPoint(0,21),wxSize(200,150),wxTE_MULTILINE);

    helpMenu->Append(Minimal_About, _T("&About...\tF1"), _T("Show about dialog"));

    fileMenu->Append(Minimal_Quit, _T("E&xit\tAlt-X"), _T("Quit this program"));
	txtHint =new wxTextCtrl(this, Gui_hintText,s,wxPoint(201,21),wxSize(200,150),wxTE_MULTILINE);

    // now append the freshly created menu to the menu bar...
    wxMenuBar *menuBar = new wxMenuBar();
    menuBar->Append(fileMenu, _T("&File"));
    menuBar->Append(helpMenu, _T("&Help"));

    // ... and attach this menu bar to the frame
    SetMenuBar(menuBar);
#endif // wxUSE_MENUS

#if wxUSE_STATUSBAR
    // create a status bar just for fun (by default with 1 pane only)
    CreateStatusBar(2);
    SetStatusText(_T("H^3 By Omar Baqueiro Espinosa"));
#endif // wxUSE_STATUSBAR
	Hide();
}

// set the text of the text control
void MyFrame::add_text(char *text)
{
	wxString newText;
	for (int i=0;i<strlen(text);i++){

		if (text[i]=='\n')
			newText.Append(_T("\r\n"));
		else
			newText.Append(text[i],1);

	}

	txtMatrix->AppendText(newText);
	return;

}
void MyFrame::set_hint_text(char *text)
{
		wxString newText;
	for (int i=0;i<strlen(text);i++){

		if (text[i]=='\n')
			newText.Append(_T("\r\n"));
		else
			newText.Append(text[i],1);

	}

	txtHint->SetValue(newText);
	return;
}

// add text to the hint text control
void MyFrame::add_hint_text(char *text)
{
	wxString newText;
	for (int i=0;i<strlen(text);i++){

		if (text[i]=='\n')
			newText.Append(_T("\r\n"));
		else
			newText.Append(text[i],1);

	}

	txtHint->AppendText(newText);
	return;
}

void MyFrame::set_cursor_text(char *text)
{

	wxString newText;
	for (int i=0;i<strlen(text);i++){
		if (text[i]=='\n')
			newText.Append(_T("\r\n"));
		else
			newText.Append(text[i],1);
	}
	lblCursorPosition->SetLabel(newText);
}

// ----------------------------------------------------------------------------
// event tables and other macros for wxWidgets
// ----------------------------------------------------------------------------

// the event tables connect the wxWidgets events with the functions (event
// handlers) which process them. It can be also done at run-time, but for the
// simple menu events like this the static method is much simpler.
BEGIN_EVENT_TABLE(MyFrame, wxFrame)
    EVT_MENU(Minimal_Quit,  MyFrame::OnQuit)
    EVT_MENU(Minimal_About, MyFrame::OnAbout)
END_EVENT_TABLE()

// Create a new application object: this macro will allow wxWidgets to create
// the application object during program execution (it's better than using a
// static object for many reasons) and also implements the accessor function
// wxGetApp() which will return the reference of the right type (i.e. MyApp and
// not wxApp)
IMPLEMENT_APP(MyApp)
