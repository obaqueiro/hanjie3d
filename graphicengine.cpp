/*
 Part of Hanjie3D project
 
 Released under the GPL 2.0 License (See file gpl-2.0.txt or http://www.gnu.org/licenses/gpl-2.0.txt)
 Copyright (2006 - 2011)
 Author: Omar Baqueiro Epinosa

*/

#include "graphicengine.h" // class's header file
#include <gl/gl.h>
#include <gl/glu.h>
#include <gl/glext.h>
#include "Scenario.h"
#include "camera.h"
#include "scenario3d.h"
#include <SDL/SDL_TTF.H>
// class destructor
GraphicEngine::~GraphicEngine()
{

}

GraphicEngine  & GraphicEngine::get_handle()
{
	// Singleton Graphic Engine object
	static GraphicEngine g;
	return g;
}
// Initialize graphic SDL+OpenGL system
int GraphicEngine::init_graphics(bool fullscreen)
{
	int width;
	int height;
	int bpp;
	int flags;

	width = SCREEN_WIDTH;
	height =  SCREEN_HEIGHT;
	bpp = SCREEN_BPP;


	// Set OpenGL attributes
	SDL_GL_SetAttribute (SDL_GL_RED_SIZE, 5);
	SDL_GL_SetAttribute (SDL_GL_BLUE_SIZE, 5);
	SDL_GL_SetAttribute (SDL_GL_GREEN_SIZE,5);
	SDL_GL_SetAttribute (SDL_GL_DEPTH_SIZE,16);
	SDL_GL_SetAttribute (SDL_GL_DOUBLEBUFFER,1);

	// set sdl flags
	flags = SDL_OPENGL ;
	if (fullscreen == true){
		flags |= SDL_FULLSCREEN;
	}
	SDL_WM_SetCaption ("H^3 - by Omar Baqueiro","images/h3.ico");
	SDL_WM_SetIcon(SDL_LoadBMP("images/h3.bmp"),NULL);
	// set video mode
	this->screen = SDL_SetVideoMode (width, height, bpp, flags);

	if (screen == NULL)	{
		fprintf(stderr, "Video mode set failed: %s\n",
			SDL_GetError());
			return -1;
	}
	// initialize SDL_TTF
	TTF_Init();


	setup_opengl();
	return 0;
}

void GraphicEngine::draw_scene( void )
{


    /* Clear the color and depth buffers. */
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    /* We don't want to modify the projection matrix. */
    glMatrixMode( GL_MODELVIEW );


	// get scenario graphical system singleton
	Scenario3D &scenario3d = Scenario3D::get_handle() ;
	Camera &camera = Camera::get_handle();

	glLoadIdentity( );

	glTranslatef(camera.xpos, camera.ypos, camera.zpos);
	// draw scenario
	scenario3d.draw();

		// Position The Light
	//glLightfv(GL_LIGHT1, GL_POSITION,Scenario::get_handle().lightPosition);


	// swap OpenGL buffer using
    SDL_GL_SwapBuffers( );
    update_FPS();
	}

void GraphicEngine::setup_opengl()
{
	 float ratio = (float) SCREEN_WIDTH / (float) SCREEN_HEIGHT;

    /* Our shading model--Gouraud (smooth). */
    glShadeModel( GL_SMOOTH );

    /* Set the clear color. */
    glClearColor( 0.0f, 0.5f, 0.5f, 1.0 );

    /* Setup our viewport. */
    glViewport( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );


	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glEnable(GL_LIGHTING);
	glEnable(GL_BLEND);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_TEXTURE_2D);// Enable 2D textures
	// Blending Function For Translucency Based On Source Alpha Value
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA	);

	// setup lighting
	setup_lights();


	// Really Nice Perspective Calculations
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    /*
     * Change to the projection matrix and set
     * our viewing volume.
     */
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity( );
    /*
     * EXERCISE:
     * Replace this with a call to glFrustum.
     */
    gluPerspective( 70.0, ratio, 10.0, 1024.0 );

}

int GraphicEngine::power_of_two(int input)
{
	int value = 1;

	while ( value < input ) {
		value <<= 1;
	}
	return value;
}

GLuint GraphicEngine::load_texture(SDL_Surface *surface, GLfloat *texcoord)
{
	GLuint texture;
	int w, h;
	SDL_Surface *image;
	SDL_Rect area;
	Uint32 saved_flags;
	Uint8  saved_alpha;

	/* Use the surface width and height expanded to powers of 2 */
	w = power_of_two(surface->w);
	h = power_of_two(surface->h);
	texcoord[0] = 0.0f;			/* Min X */
	texcoord[1] = 0.0f;			/* Min Y */
	texcoord[2] = (GLfloat)surface->w / w;	/* Max X */
	texcoord[3] = (GLfloat)surface->h / h;	/* Max Y */

	image = SDL_CreateRGBSurface(
			SDL_SWSURFACE,
			w, h,
			32,
#if SDL_BYTEORDER == SDL_LIL_ENDIAN /* OpenGL RGBA masks */
			0x000000FF,
			0x0000FF00,
			0x00FF0000,
			0xFF000000
#else
			0xFF000000,
			0x00FF0000,
			0x0000FF00,
			0x000000FF
#endif
		       );
	if ( image == NULL ) {
		return 0;
	}

	/* Save the alpha blending attributes */
	saved_flags = surface->flags&(SDL_SRCALPHA|SDL_RLEACCELOK);
	saved_alpha = surface->format->alpha;
	if ( (saved_flags & SDL_SRCALPHA) == SDL_SRCALPHA ) {
		SDL_SetAlpha(surface, 0, 0);
	}

	/* Copy the surface into the GL texture image */
	area.x = 0;
	area.y = 0;
	area.w = surface->w;
	area.h = surface->h;
	SDL_BlitSurface(surface, &area, image, &area);

	/* Restore the alpha blending attributes */
	if ( (saved_flags & SDL_SRCALPHA) == SDL_SRCALPHA ) {
		SDL_SetAlpha(surface, saved_flags, saved_alpha);
	}

	/* Create an OpenGL texture for the image */
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D,
		     0,
		     GL_RGBA,
		     w, h,
		     0,
		     GL_RGBA,
		     GL_UNSIGNED_BYTE,
		     image->pixels);
	SDL_FreeSurface(image); /* No longer needed */

	return texture;
}
 SDL_Surface * GraphicEngine::load_bitmap(char * filename)
{
	SDL_Surface *image;

    /* Load the BMP file into a surface */
    image = SDL_LoadBMP(filename);
    if (image == NULL) {
        fprintf(stderr, "Couldn't load %s: %s\n", filename, SDL_GetError());
        return NULL;
    }
    return image;
}

// Set the OpenGL active texture to use
void GraphicEngine::enable_texture(GLuint texture)
{
	glBindTexture(GL_TEXTURE_2D,texture);
}

void GraphicEngine::setup_lights()
{
	glLightfv(GL_LIGHT1, GL_AMBIENT,Scenario::get_handle().ambientLight );
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,Scenario::get_handle().ambientLight );
	GLfloat whitelight[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, whitelight);

	// Setup The Diffuse Light
	//glLightfv(GL_LIGHT1, GL_DIFFUSE, Scenario::getHandle()->diffuseLight);
	//glLightfv(GL_LIGHT1, GL_POSITION,Scenario::getHandle()->lightPosition); // Position The Light
	glEnable(GL_LIGHT1);


}
GLuint GraphicEngine::generate_GL_text(char *text, GLuint texId=0, bool horizAlign=true)
{

	int w,h;
	SDL_Color color = {255,255,255};
	TTF_Font* font;
	SDL_Surface *initialSurface;
	SDL_Surface *resultSurface;

	font = TTF_OpenFont(STR_TTF_FONT_FILE, 14);}
	if (horizAlign==true){
        initialSurface = TTF_RenderText_Solid   (font, text, color);
	}
	else
	{

	}

	// get the next power-of-two dimensions for OpenGL textures
	w=1;
	h=1;
	while (w<initialSurface->w || h<initialSurface->h){
		if (w<initialSurface->w){
			w=w<<1;
		}
		if (h<initialSurface->h){
			h=h<<1;
		}
	}
	// the pow-of-2 surface  to generate OpenGL texture
	resultSurface = SDL_CreateRGBSurface(0, w, h, 32,
			0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000);
//	SDL_CreateRGBSurface(0, w, h, 24,
//			0xff000000, 0x00ff0000, 0x0000ff00, 0);

	// blit text surface
	SDL_BlitSurface(initialSurface, 0, resultSurface, 0);

	// generate OpenGL texture
	if (texId==0) {
	    glGenTextures(1, &texId);
	}
	glBindTexture(GL_TEXTURE_2D, texId);
	glTexImage2D(GL_TEXTURE_2D, 0, 4, w, h, 0, GL_BGRA,
			GL_UNSIGNED_BYTE, resultSurface->pixels );

    glFinish();
	// delete surfaces and font
	SDL_FreeSurface(resultSurface);
	SDL_FreeSurface(initialSurface);
	TTF_CloseFont(font);
	return texId;
}

void GraphicEngine::update_FPS()
{
    static Uint32 lastTime=0;
    static Uint32 frameCount=0;
    static Uint32 elapsedTime=0;

    frameCount++;
    Uint32 delta = SDL_GetTicks() - lastTime;

    if (delta>=1000){
        FPS = 1000*((float)frameCount/delta);
        frameCount=0;
        elapsedTime=0;
        lastTime=SDL_GetTicks();
    }


}

float GraphicEngine::get_FPS(){
    return FPS;
}
