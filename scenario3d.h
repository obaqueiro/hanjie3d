/*
 Part of Hanjie3D project
 
 Released under the GPL 2.0 License (See file gpl-2.0.txt or http://www.gnu.org/licenses/gpl-2.0.txt)
 Copyright (2006 - 2011)
 Author: Omar Baqueiro Epinosa

*/

#ifndef SCENARIO3D_H
#define SCENARIO3D_H

#include <gl/gl.h>
#include <gl/glu.h>
#include "cube3d.h"
#include "graphicengine.h"
#include "scenario.h"
#include <list>
/*
 * Contains the functions that draw the scenario on the screen
 * related to OpenGL
 */
class Scenario3D
{

	// class constructor
	Scenario3D();
	Scenario3D(Scenario3D const&){}    // copy ctor hidden
	Scenario3D& operator=(Scenario3D const&){return get_handle();}  // assign op hidden

	// The texture for the used cube type
	GLuint surfUsedCube;
	// The surface texture for the unused cube type
	GLuint surfUnusedCube;
	// Background texture
	GLuint surfBackground;

	// Load the textures to use in the different types of cubes
	void load_textures();

	void draw_background();


	// draw the scenario hint numbers
	void draw_hints();

	// Display list for the hints texture
    int hintDisplayList ;

	GLuint *hintRow; // array of hints row GL textures
	int hintRowMax, hintColMax; // total number of rows and column hints
	GLuint *hintCol; // array of hints column GL textures
	public:

        void update_hints(bool force); // update the hint textures
		// rotate the scenario on the X axis
		void rotate_x(int degrees);
		// rotate the scenario on the Y axis
		void rotate_y(int degrees);
		// rotate the scenario on the Z axis
		void rotate_z(int degrees);
	    GLfloat rotationMatrix[16]  ;
		// class destructor
		~Scenario3D();

		// returns singleton instance handle
		static Scenario3D &get_handle()
		{
			static Scenario3D s; // the only scenario3d instance
			return s;
		}

		// draw the scenario
		void draw();

};

#endif // SCENARIO3D_H
