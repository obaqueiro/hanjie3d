/*
 Part of Hanjie3D project
 
 Released under the GPL 2.0 License (See file gpl-2.0.txt or http://www.gnu.org/licenses/gpl-2.0.txt)
 Copyright (2006 - 2011)
 Author: Omar Baqueiro Epinosa

*/
#ifndef MY_FRAME
#define MY_FRAME

#include "main.h"

// Define a new frame type: this is going to be our main frame
class MyFrame : public wxFrame
{
public:
    // ctor(s)
    MyFrame(const wxString& title);

    // event handlers (these functions should _not_ be virtual)
    void OnQuit(wxCommandEvent& event);
    void OnAbout(wxCommandEvent& event);

	// set the text of the GUI textbox
	void add_text(char *text);
	void set_cursor_text(char *txt);
	void set_hint_text(char *txt);
	void add_hint_text(char *txt);
	static MyFrame &get_handle();

private:

	wxTextCtrl *txtMatrix;
	wxStaticText *lblCursorPosition;
	wxTextCtrl *txtHint;
    // any class wishing to process wxWidgets events must use this macro
    DECLARE_EVENT_TABLE()

};

#endif
