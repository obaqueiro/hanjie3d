
Hanjie^3
Por Omar Baqueiro Espinosa [obaqueiro@_NOSPAM_gmail.com] [sin _NOSPAM_]

�ltima revisi�n: 04 de Diciembre del 2005

Contenido:

	Introducci�n
	Acerca del programa
	Instrucciones
	Historia de versiones
	Miscelaneos
	
-------------------------------------------------------------------------------
Introducci�n

Hanjie^3 es una extensi�n en 3 dimensiones del cl�sico juego japon�s llamado 
Hanjie o com�nmente conocido como nonograma o "pintar con n�meros".

T�picamente este juego consiste en rellenar cuadros de acuerdo con el patr�n
indicado por los n�meros de las orillas. 

As�, por ejemplo en la siguiente "imagen" se puede ver un tablero de nonograma
de 5x4:
       
      1   1
      1 2 1 1 1
      ___________
  1 1 |_|_|_|_|_|
    3 |_|_|_|_|_|
    2 |_|_|_|_|_|
    1 |_|_|_|_|_|
      
Y su soluci�n seria: 
       1   1
       1 2 1 1 1
      ___________
  1 1 |O|X|X|X|O|
    3 |X|O|O|O|X|
    2 |O|O|X|X|X|
    1 |X|X|O|X|X|
    
Donde las O's son los cuadros que si van pintados y las X son los que no. 
Como ejemplo, t�mese el rengl�n 1 el cual comienza con "1 1", lo que 
significa que en ese rengl�n hay 1 cuadro pintado y despu�s otro cuadro 
pintado separado. Para poder saber d�nde va cada uno de ellos s necesario 
ver los n�meros de las columnas ya que al final la cantidad de cuadros 
pintados para las columnas y renglones deben de "cuadrar".
	
-------------------------------------------------------------------------------
Acerca del programa

El juego H^3 consiste de un escenario con 3 dimensiones (alto, ancho y 
profundidad). Cada escenario consta de varios cubos y de la misma manera en que
se llenan o tachan los cuadros en el juego cl�sico de nonograma aqu� se pueden
seleccionar los cuadros como "usado" o "no usado" en la figura.

El programa est� hecho utilizando las librer�as OpenGL, wxWidgets y SDL. Esto
con el fin de hacerlo totalmente portable. El c�digo puede ser compilado y
corrido en Windows, Linux y Mac.


-------------------------------------------------------------------------------
Instrucciones

A continuaci�n se describen la lista de teclas disponibles y sus movimientos:

Movimiento del cursor:

[Arriba],[Abajo]: 			Mueve el cursor hacia el cubo de arriba o de abajo.
[Izquierda],[Derecha]: 	Mueve el cursor hacia el cubo de la izquiera o derecha.
[Q],[W]:								Mueve el cursor hacia el cubo de atr�s o de adelante.

[ALT]+[Arriba]:			Rota el escenario hacia arriba.
[ALT]+[Abajo]:			Rota el escenario hacia abajo.
[ALT]+[Derecha]:		Rota el cursor hacia la derecha.
[ALT]+[Izquierda]:	Rota el cursor hacia la izqueirda.

Modificaci�n del escenario:

[Fin]:			Aumenta el espacio entre cada cubo.
[Inicio]:		Disminuye el espacio entre cada cubo.

[X]:				Marca el cubo seleccionado como "No utilizado".
[C]:				Marca el cubo seleccionado como "Utilizado".
[Z]:				Pone el cubo seleccionado en el estado neutro.
[ESPACIO]:  Alterna entre los 3 estados del cubo.

Otras teclas:

[ESC]:			Sale del juego
			


-------------------------------------------------------------------------------
Historia de versiones

_______________________________________________________________________________
v 0.0.6 [04 - Diciembre - 2005]

Ok, esta es la tercera versi�n semi-p�blica. Esta versi�n es la primera que
puede ser "jugada". Es decir todos los elementos m�nimos para jugar est�n
programados.

Esta versi�n contiene bastantes cosas nuevas:

- Deslpegado de n�meros correspondientes a filas y columnas.
- Actualizado del algoritmo de carga de escenario
- Actualizado del algoritmo de transparencia
- Se agregaron 2 par�metros para la linea de comando (sin comillas):
   "-cheat1" : Muestra los cubos utilizados por la figura
   "-full"   : inicia el programa en pantalla completa

- Se arrelgaron varios bugs
...
++++++++++++++
Siguiente version: [0.0.7]
Estas son las caracter�sticas en las que trabajar� para la siguiente versi�n:

- Posibilidad de cargar escenario
- Mostrar los n�meros correspondientes a las columnas de manera vertical

	
_______________________________________________________________________________
v 0.0.5 [15 - Octubre - 2005]

Ok, esta es la segunda versi�n semi-p�blica. Esta versi�n contiene bastantes 
cosas nuevas:

- Texturas: 						Los cubos del escenario han sido texturizados.
- Transparencia: 				El escenario cuenta con transparencia para diferenciar 
												el estado actual de cada cubo.
- Indicador de cursor: 	Se puede ver el cubo seleccionado.
- Movimiento de cursor: Se puede mover el cursor para seleccionar otro cubo.
- Fondo: 								Un fondo preliminar para que se vea un poco m�s din�mico
												el programa.
- Movimiento del cubo:	�Por fin! se puede mover de manera adecuada el cubo con 
												la tecla ALT + las flechas direccionales.
- Modificaci�n 
  de estado:						Se puede modificar el estado del cubo seleccionado.
- Espacio entre cubos:	Se puede modificar el espacio entre cada cubo.
...

Adem�s de muchos cambios al c�digo fuente. 

++++++++++++++
Siguiente version: [0.0.7]
Estas son las caracter�sticas en las que trabajar� para la siguiente versi�n:

- Posibilidad de cargar escenario
- Mostrar los n�meros del escenario. Evidentemente es lo �nico que falta para 
	poder utilizarlo de manera m�nimca.
--------------------------------------------------------------------------------

Miscelaneos

Este programa est� en estado de "alpha" todav�a. De hecho, en esta versi�n 
[0.0.5] ni siquiera se puede jugar. Si se tiene alguna pregunta o sugerencia
puede contactarme a mi direcci�n de correo.

Adem�s, el programa lo estoy haciendo en mis pocos tiempos libres por lo que
el avance no es muy r�pido. 

Si le�ste hasta ac�, que bien. Seg�n este documento la posibilidad de cargar 
un escenario no est� disponible pero si es posible cargar otros escenarios.
Para hacer esto solo tienes que renombrar el archivo "scenario2.han" a
"scenario2.han.bak" y luego renombrar el archivo "scenario1.han" a 
"scenario2.han". Con esto puedes ver otro escenario cuando corras el juego.


