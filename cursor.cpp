/*
 Part of Hanjie3D project
 
 Released under the GPL 2.0 License (See file gpl-2.0.txt or http://www.gnu.org/licenses/gpl-2.0.txt)
 Copyright (2006 - 2011)
 Author: Omar Baqueiro Epinosa

*/

#include "cursor.h" // class's header file
#include "scenario.h"
// class constructor
Cursor::Cursor()
{
		x=y=z=0;

		scaleFactor=1.0f;
		scaleDir=1;

}

// class destructor
Cursor::~Cursor()
{
	// insert your code here
}
void Cursor::move_left(int units)
{
Scenario &scenario = Scenario::get_handle();

	x=(x-(units*scenario.rotationMatrix[0][0]))%scenario.get_width();
	y=(y-(units*scenario.rotationMatrix[0][1]))%scenario.get_height();
	z=(z-(units*scenario.rotationMatrix[0][2]))%scenario.get_depth();
	if (x<0) {
		x=scenario.get_width()-1;
	}
	if (y<0){
		y=scenario.get_height()-1;
	}

	if (z<0){
		z=scenario.get_depth()-1;
	}

}
void Cursor::move_right(int units)
{
	Scenario &scenario = Scenario::get_handle();

	x=(x+(units*scenario.rotationMatrix[0][0]))%scenario.get_width();
	y=(y+(units*scenario.rotationMatrix[0][1]))%scenario.get_height();
	z=(z+(units*scenario.rotationMatrix[0][2]))%scenario.get_depth();
	if (x<0) {
		x=scenario.get_width()-1;
	}
	if (y<0){
		y=scenario.get_height()-1;
	}

	if (z<0){
		z=scenario.get_depth()-1;
	}
}
void Cursor::move_up(int units)
{
	Scenario &scenario = Scenario::get_handle();
	x=(x+(units*scenario.rotationMatrix[1][0]))%scenario.get_width();
	y=(y+(units*scenario.rotationMatrix[1][1]))%scenario.get_height();
	z=(z+(units*scenario.rotationMatrix[1][2]))%scenario.get_depth();
	if (x<0) {
		x=scenario.get_width()-1;
	}
	if (y<0){
		y=scenario.get_height()-1;
	}

	if (z<0){
		z=scenario.get_depth()-1;
	}


}
void Cursor::move_down(int units)
{
	Scenario &scenario = Scenario::get_handle();
	x=(x-(units*scenario.rotationMatrix[1][0]))%scenario.get_width();
	y=(y-(units*scenario.rotationMatrix[1][1]))%scenario.get_height();
	z=(z-(units*scenario.rotationMatrix[1][2]))%scenario.get_depth();
	if (x<0) {
		x=scenario.get_width()-1;
	}
	if (y<0){
		y=scenario.get_height()-1;
	}

	if (z<0){
		z=scenario.get_depth()-1;
	}
}
void Cursor::move_forward(int units)
{
	Scenario &scenario = Scenario::get_handle();

	x=(x+(units*scenario.rotationMatrix[2][0]))%scenario.get_width();
	y=(y+(units*scenario.rotationMatrix[2][1]))%scenario.get_height();
	z=(z+(units*scenario.rotationMatrix[2][2]))%scenario.get_depth();
	if (x<0) {
		x=scenario.get_width()-1;
	}
	if (y<0){
		y=scenario.get_height()-1;
	}

	if (z<0){
		z=scenario.get_depth()-1;
	}
scenario.update_hints(true); // update the hints

}
void Cursor::move_backward(int units)
{
		Scenario &scenario = Scenario::get_handle();

	x=(x-(units*scenario.rotationMatrix[2][0]))%scenario.get_width();
	y=(y-(units*scenario.rotationMatrix[2][1]))%scenario.get_height();
	z=(z-(units*scenario.rotationMatrix[2][2]))%scenario.get_depth();
	if (x<0) {
		x=scenario.get_width()-1;
	}
	if (y<0){
		y=scenario.get_height()-1;
	}

	if (z<0){
		z=scenario.get_depth()-1;
	}
		scenario.update_hints(true); // update the hints
}

float Cursor::get_scale_factor()
{
	if (scaleFactor>2 || scaleFactor <0.1 || scaleDir <-1 || scaleDir>1) {
		scaleFactor =1;
		scaleDir=1;
	}
	if (scaleDir==1){
		if (scaleFactor<1.2){
			scaleFactor+=0.02f;
		}
		else {
			scaleDir=-1;
		}
	}
	else {
		if (scaleFactor>.9) {
			scaleFactor-=0.02f;
		}
		else {
			scaleDir=1;
		}
	}
	return scaleFactor;
}

void Cursor::update()
{
		char text[80];
		sprintf (text, "x: %d, y: %d, z: %d", x,y,z);
}
void Cursor::set_x(int newX){
	Scenario &scenario = Scenario::get_handle();
	if (newX>0&& newX<scenario.get_width()){
		x=newX;
	}
}
void Cursor::set_y(int newY){
	Scenario &scenario = Scenario::get_handle();
	if (newY>0&& newY<scenario.get_height()){
		y=newY;
	}
}
void Cursor::set_z(int newZ){
	Scenario &scenario = Scenario::get_handle();
	if (newZ>0&& newZ<scenario.get_depth()){
		z=newZ;
	}
}
